package user;

import inventory.DatabaseCon;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ViewReceived
 */
public class ViewReceived extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession session;
	DatabaseCon con1 = new DatabaseCon();
    Connection con;
    ResultSet res;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewReceived() {
        super();
        con=con1.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			session=request.getSession();
			String uname= (String)session.getAttribute("uname");
			String query="select * from issue where incharge = '"+uname+"'";// where location in (select deptID from division where in_mgr ='"+uname+"')";
			res=con1.getResult(query);
			
			if(res.next()){
				request.setAttribute("res", res);
				RequestDispatcher rd = request.getRequestDispatcher("/User/staffSearch.jsp");
				rd.forward(request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
