package inventory;

import java.io.IOException;
//import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
/**
 * Servlet implementation class AddUser
 */
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String fname, lname, uname,roleId, dept, mail;
	int tp_no;
	DatabaseCon con;
	ResultSet res;
	AddUserQuery addQ1, addQ2;
	boolean valid = false;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUser() {
        super();
        try{
        con= new DatabaseCon();
        con.getConnection();
        }catch (Exception e){
        	System.out.print(e);
        }
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		//PrintWriter out= response.getWriter();
		try{
		fname = request.getParameter("fname");
		lname = request.getParameter("lname");
		mail = request.getParameter("mail");
		uname= request.getParameter("uname");
		tp_no= Integer.parseInt(request.getParameter("tpno"));
		dept=request.getParameter("department");
		roleId = request.getParameter("level");
	//out.print(roleId);
		String query="select username from login where username= '"+uname+"'";
		res=con.getResult(query);
		if(!res.next()){
		query = "select deptID from division where deptName = '"+dept+"'";
		res=con.getResult(query);
		
		if(res.next()){
		dept=res.getString("deptID");
		}
		
		System.out.print(dept);
		
		addQ1 = new AddUserQuery(fname,lname,mail,dept,10,uname);
		addQ1.InsertUsers();
		
		addQ2 = new AddUserQuery(uname,roleId);
		addQ2.InsertLogin();
		
		request.setAttribute("valid", "Added Sucess");
		RequestDispatcher rd = request.getRequestDispatcher("/adduser.jsp");
		rd.forward(request, response);
		//response.sendRedirect("adduser.jsp?valid");
		}else{
			request.setAttribute("uname", "User name is already used");
			RequestDispatcher rd = request.getRequestDispatcher("/adduser.jsp");
			rd.forward(request, response);
		}
		}catch(Exception e){
			System.out.print(e);
		}
	}

}
