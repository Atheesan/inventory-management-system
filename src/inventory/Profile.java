package inventory;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import java.sql.*;
/**
 * Servlet implementation class Profile
 */
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon con1 = new DatabaseCon();
    Connection con;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        con=con1.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		try{
			
		String uname = (String)request.getSession().getAttribute("uname");//request.getParameter("uname");
		//String  tpno =request.getParameter("tpno");
		String fname= request.getParameter("fname");
		String lname= request.getParameter("lname");
		String mail= request.getParameter("mail");
		String tpno= request.getParameter("tpno");
		
		PreparedStatement pst = con.prepareStatement("update users set fname='"+fname+"',lname='"+lname+"',mail='"+mail+"',tp_no='"+tpno+"' where uId = '"+uname+"'");
		int i= pst.executeUpdate();
		if(i!=0){  
			System.out.print("Successfully Edited"); 
			}
		
		request.setAttribute("valid", "Profile Edited");
		if(request.getParameter("staffprofile")!=null){
		RequestDispatcher rd = request.getRequestDispatcher("User/staffProfile.jsp");
		rd.forward(request, response);
		}
		else if(request.getParameter("deanprofile")!=null){
			RequestDispatcher rd = request.getRequestDispatcher("Dean/DeanProfile.jsp");
			rd.forward(request, response);
		}
		}catch(Exception e){
		 	e.printStackTrace();
		}
		}
	}


