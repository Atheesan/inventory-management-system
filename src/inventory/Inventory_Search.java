package inventory;

import java.io.IOException;
//import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class Inventory_Search
 */
public class Inventory_Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon dbcon;
	Connection con;
	ResultSet res;
	SearchQuery sq;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inventory_Search() {
        super();
        dbcon = new DatabaseCon();
        con= dbcon.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html");  
		//PrintWriter pw = response.getWriter();
		
		try{
			String category = request.getParameter("category");
			//String item = request.getParameter("txtcode");
			System.out.println(category);
			sq=new SearchQuery();
			if(category.equalsIgnoreCase("Consumable Items")){
			res=sq.staionerySearch();
			
			}else if(category.equalsIgnoreCase("Fixed Assets")){
			res=sq.permanentSearch();	
			}
			
			if(res.next()){
				request.setAttribute("category", category);
				request.setAttribute("res", res);
				/**if(request.getParameter("cmdsearchStaff")!=null){
					RequestDispatcher rd = request.getRequestDispatcher("/User/staffViewAvailable.jsp");
					rd.forward(request, response);
				}*/
				if(request.getParameter("searchStaff")!=null){
					RequestDispatcher rd = request.getRequestDispatcher("/User/staffViewAvailable.jsp");
					rd.forward(request, response);
				}
				else if(request.getParameter("admin")!=null){
				RequestDispatcher rd = request.getRequestDispatcher("/inventorySearch.jsp");
				rd.forward(request, response);
			}
				else if(request.getParameter("hodsearch")!=null){
					RequestDispatcher rd = request.getRequestDispatcher("/HOD/HODViewAvailable.jsp");
					rd.forward(request, response);
				}	
				else if(request.getParameter("deansearch")!=null){
					RequestDispatcher rd = request.getRequestDispatcher("/Dean/DeanViewAvailable.jsp");
					rd.forward(request, response);
				}	
			}
			
		}catch(Exception e){
			System.out.print(e);
		}
	}

}
