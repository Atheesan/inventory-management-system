package inventory;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
/**
 * Servlet implementation class ApproveHod
 */
public class DeanApprove extends HttpServlet {
	private static final long serialVersionUID = 1L;
       String mutex="";
       ResultSet res;
       DatabaseCon con;
       Connection con1;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeanApprove() {
        super();
        con=new DatabaseCon();
      con1=  con.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String mailId="darsha.m3@gmail.com";
		HttpSession session=request.getSession();
		String uname= (String)session.getAttribute("uname");
		
		try{
			if(request.getParameter("deanview")!=null){
				String req = request.getParameter("reqid");
				String query;
				query="select * from request where req_id = '"+req+"'";
				res=con.getResult(query);
				if(res.next()){
					
					request.setAttribute("reqid", req);
					request.setAttribute("name", res.getString("item_name"));
					//request.setAttribute("code", res.getString("code"));
					request.setAttribute("quantity", res.getString("quantity"));
					request.setAttribute("incharge", res.getString("username"));
					
					RequestDispatcher rd = request.getRequestDispatcher("/Dean/DeanApprove.jsp");
					rd.forward(request, response);
				
			}
			}
		if(request.getParameter("forward")!=null){
			String reqid= request.getParameter("reqid");
			
			String query="update request set status = 'Accepted by Dean' where req_id = '"+reqid+"' ";
			PreparedStatement ps=con1.prepareStatement(query);
			int i= ps.executeUpdate();
			if(i!=0){
			synchronized (mutex){
			//String	 query="select mail from users where uId in (select HOD from division where deptID = '"+dept+"')";
				//res= con.getResult(query);
				//while(res.next()){
					// mailId =(res.getString("mail"));
					 SendMail mail=new SendMail();
						mail.mailing(mailId,"",uname);
				//}
		}
			PrintWriter out = response.getWriter();  
			response.setContentType("text/html");  
			out.println("<script type=\"text/javascript\">");  
			out.println("var ans = alert('Request has been Forwared');");
			out.println("if(!ans){window.location=\"http://localhost:8080/inventoy_management/Dean/DeanviewRequest.jsp\"}");
			out.println("</script>");
			}
	}
		}catch(Exception e){
			e.printStackTrace();
		}
}
}
