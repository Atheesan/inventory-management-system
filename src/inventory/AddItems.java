package inventory;

import java.io.*;

//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class AddItems
 */
public class AddItems extends HttpServlet {
	/*DatabaseConnection dbc;
	Connection con;
	ResultSet rs;*/
	String id, name, desc, query, bal,m,mi,re;
	int max, min, reorder, balance;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItems() {
        super();
      /**  try{
    		Class.forName("com.mysql.jdbc.Driver").newInstance();
    	con = DriverManager.getConnection("jdbc:mysql://localhost/Inventory","root","");
    	//System.out.println("Success");
    		}
    	catch(Exception e){
    		System.out.print("could not connect");
    	}*/
                     
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
response.setContentType("text/html");  
PrintWriter pw = response.getWriter();  
String connectionURL = "jdbc:mysql://localhost/inventory_control";// newData is the database  
Connection connection;  
try{  
String code= request.getParameter("id");  
String name = request.getParameter("name"); 
bal=request.getParameter("balance");
balance=Integer.parseInt(bal);
m=request.getParameter("maximum");
max=Integer.parseInt(m);
mi=request.getParameter("minimum");
min=Integer.parseInt(mi);
re=request.getParameter("reorder");
reorder=Integer.parseInt(re);
desc=request.getParameter("description");
 

Class.forName("com.mysql.jdbc.Driver");  
connection = DriverManager.getConnection(connectionURL, "root", "");  
PreparedStatement pst = connection.prepareStatement("insert into items(code,item_name,maximum_level,minimum_level,reorder_level,description) values(?,?,?,?,?,?)");//try2 is the name of the table  
pst.setString(1,code);  
pst.setString(2,name);
//pst.setInt(3,balance);  
pst.setInt(3,max);  
pst.setInt(4,min);  
pst.setInt(5,reorder);  
pst.setString(6,desc);  
int i = pst.executeUpdate();  
if(i!=0){  
pw.println("<br>Record has been inserted");  


}  
else{  
pw.println("failed to insert the data");  
}  
}  
catch (Exception e){  
pw.println(e);  
}  
}  
}  



