package inventory;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
/**
 * Servlet implementation class ViewItems
 */
public class ViewItems extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon con1 = new DatabaseCon();
       Connection con;
       ResultSet res;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewItems() {
        super();
        con=con1.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try{
			
			String query="select item,code,quantity from issue";
			

	
			res=con1.getResult(query);
			
			if(res.next()){
				request.setAttribute("res", res);
				RequestDispatcher rd = request.getRequestDispatcher("/User/staffSearch.jsp");
				rd.forward(request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}

