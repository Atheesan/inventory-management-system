package inventory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Recieve
 */
public class Recieve extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon con1;
    Connection con;
    ResultSet res,res1;
    String name,uname,quant,dept;
    
    public Recieve() {
        super();
        con1=new DatabaseCon();
        con=  con1.getConnection();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String req = request.getParameter("reqid");
		try{
		
		
		if (request.getParameter("view") != null) {
			String query="select * from issue where issue_id = '"+req+"'";
			res=con1.getResult(query);
			if(res.next()){
				
				request.setAttribute("reqid", req);
				request.setAttribute("name", res.getString("item"));
				//request.setAttribute("code", res.getString("code"));
				request.setAttribute("quantity", res.getString("incharge"));
				request.setAttribute("incharge", res.getString("quantity"));
				RequestDispatcher rd = request.getRequestDispatcher("/recieve.jsp");
				rd.forward(request, response);
			}
		} else if (request.getParameter("issue") != null) {
	    	  Recieve del = new Recieve();
	    	  del.recieveItem(req);
	    	  RequestDispatcher rd = request.getRequestDispatcher("/recieve.jsp");
				rd.forward(request, response);
	      }
		}catch(Exception e){
			e.printStackTrace();}
	}

	public void recieveItem(String req){
		try{
			String query = "Delete from issue where issue_id = '"+req+"'"; //delete* from request where req_id= '"+req+"
		PreparedStatement pst=con.prepareStatement(query);
		pst.execute();
		}
			 
	catch(Exception e){
		e.printStackTrace();
	}
	}
}
