package inventory;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class UserManage
 */
public class UserManage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon con;
	ResultSet res;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserManage() {
        super();
        try{
        con=new DatabaseCon();
        con.getConnection();
        }catch(Exception e){
        	System.out.print("connectin error");
        }
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		try{
		String query = "select * from users,login where users.uId= login.username";
		res = con.getResult(query);
		if(res.next()){
			request.setAttribute("res", res);
			RequestDispatcher rd = request.getRequestDispatcher("/usermanage.jsp");
			rd.forward(request, response);
		}
	}catch(Exception e){
		System.out.print("error");
	}
	}

}
