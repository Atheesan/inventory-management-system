package inventory;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import java.sql.*;

/**
 * Servlet implementation class ViewIssue
 */
public class ViewIssue extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseCon con1 = new DatabaseCon();
       Connection con;
       ResultSet res;
      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewIssue() {
        super();
        con=con1.getConnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// HttpSession session =request.getSession();
		//String uname="adminit";//(String)session.getAttribute("uname");
		try{
			String query="select * from issue";// where location in (select deptID from division where in_mgr ='"+uname+"')";
			res=con1.getResult(query);
			
			if(res.next()){
				request.setAttribute("res", res);
				RequestDispatcher rd = request.getRequestDispatcher("/viewissue.jsp");
				rd.forward(request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
