<%@ page import="java.sql.*" %>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventory Management System FIT</title>
<meta name="keywords" content="mini social, free download, website templates, CSS, HTML" />
<meta name="description" content="Mini Social is a free website template from templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/coda-slider.css" type="text/css" media="screen" charset="utf-8" />

<script src="js/jquery-1.2.6.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.3.3.js" type="text/javascript"></script>
<script src="js/jquery.localscroll-1.2.5.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.serialScroll-1.2.1.js" type="text/javascript" charset="utf-8"></script>
<script src="js/coda-slider.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">
.auto-style1 {
	font-size: xx-large;
	color: #FFFFFF;
	text-align: center;
	background-color: #000000;
}
.auto-style3 {
	font-weight: bold;
	text-align: center;
	font-size: medium;
}
.auto-style4 {
	font-weight: bold;
	text-align: center;
}
.auto-style5 {
	font-size: medium;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body bgcolor="white" style="background-image: url('Untitled.jpeg')">
<%if(request.getAttribute("message")==null){
	out.println("");}
else {
	out.println(request.getAttribute("message"));}%>
<div id="slider">
	
    <div id="templatemo_sidebar">
    	<div id="templatemo_header">
    	  <h1>Inventory Control System</h1>
    	</div> 
    	<!-- end of header -->
        
        <ul class="navigation">
            <li><a href="adminhome.jsp" >Home<span class="ui_icon home"></span></a></li>
            <li><a href="addCategory.jsp" title="Add New Inventories To The Stock">Add Inventory<span class="ui_icon aboutus"></span></a></li>
            <li><a href="inventorySearch.jsp" title="View Available Inventories In Different Categories">Inventory Search<span class="ui_icon services"></span></a></li>
            <li><a href="usermanage.jsp" title="User Management">User Management<span class="ui_icon gallery"></span></a></li>
            <li><a href="issue.jsp" title="View The Delivered Item And Make a New Delivery">Deliver Items<span class="ui_icon contactus"></span></a></li>
			<li><a href="recieve.jsp" title="Add The Returned Permanent Item to The Stock">Return Items<span class="ui_icon contactus"></span></a></li>
			<li><a href="request.jsp" title="View Received Request">Requests<span class="ui_icon contactus"></span></a></li>

      </ul>
  </div> <!-- end of sidebar -->

	<div id="templatemo_main">
    	<ul id="social_box" class="auto-style1">
            <li>Faculty Of Information Technology</li>               
        </ul>
        
        <div id="content">
        
        <!-- scroll -->
        
        	
            
            
        <!-- end of scroll -->
         <%
   String userId = (String)session.getAttribute("uname");
   if(userId == null) {
      response.sendRedirect("http://localhost:8080/inventoy_management/index.jsp");
   }
%> 	
        
       
      <%String category=(String)request.getAttribute("category");
      if(category == null){
      category="";
      }%> 
        
        <table  style="position: absolute; left: 359px; top: 189px; width: 598px; height: 445px;">
          <tr>
            <td class="auto-style3" style="height: 8px; width: 98px">Home</td>
            <td style="width: 279px; height: 8px;" class="auto-style3"><a href="changePassword.jsp">ChangePassword</a></td>
            <td class="auto-style4" style="height: 8px">
			<span class="auto-style5"><a href="logout.jsp">Sign Out</a>
              </span>
              <% out.println(session.getAttribute("uname")); %>
              </td>
          </tr>
          <tr>
            <td colspan = "3" style="height: 125px;">
			<form method="post" action="http://localhost:8080/inventoy_management/Inventory_Search" style="position: absolute; left: 2px; top: 68px; width: 597px; height: 129px;">
              <div class="auto-style1" style="height: 113px">
                <select name="category" style="position: absolute; left: 197px; top: 27px; height: 24px; width: 122px;">
                  <option><%=category %></option>
                  <option>Fixed Assets</option>
                  <option>Consumable Items</option>
                  </select>
                <input name="txtcatagory" readonly style="border-style: none; position: absolute; left: 68px; top: 28px; background-color: #201f1b; color: #FFFFFF; width: 72px;" type="text" value="Category">&nbsp;&nbsp;
                <input name="admin" style="position: absolute; left: 107px; top: 80px; width: 86px;" type="submit" value="Search">
                </div>
            </form>
              <table border=1 style="position: absolute; left: 7px; top: 203px; width: 585px;">
                <tr>
                  <th>Item</th>
                  <th>ItemCode</th>
                  <th>Balance</th>
                  <th>Description</th>
                  </tr>
                <%try{
                	ResultSet res = (ResultSet) request.getAttribute("res");
		while(res.next()){ %>
                <tr>
                  <td><%out.print(res.getString("code")); %></td>
                  <td><%out.print(res.getString("item_name")); %></td>
                  <td><%out.print(res.getString("no_of_item")); %></td>
                  <td><%out.print(res.getString("description")); %></td>
                </tr>
                <%}}catch(Exception e){
		e.printStackTrace();//System.out.print("error");
	}%>
              </table>
            </td>
          </tr>
        </table>
</div> 
        <!-- end of content -->
        
        
    
    </div> 
<!-- end of main -->
</div>

</body>
</html>