<%@ page import="java.sql.*" %>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventory Management System FIT</title>

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/coda-slider.css" type="text/css" media="screen" charset="utf-8" />

<script src="js/jquery-1.2.6.js" type="text/javascript"></script>
<script src="js/jquery.scrollTo-1.3.3.js" type="text/javascript"></script>
<script src="js/jquery.localscroll-1.2.5.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.serialScroll-1.2.1.js" type="text/javascript" charset="utf-8"></script>
<script src="js/coda-slider.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">
.auto-style1 {
	font-size: xx-large;
	color: #FFFFFF;
	text-align: center;
	background-color: #000000;
}
.auto-style3 {
	font-weight: bold;
	text-align: center;
	font-size: medium;
}
.auto-style4 {
	font-weight: bold;
	text-align: center;
}
.auto-style5 {
	font-size: medium;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body bgcolor="white" style="background-image: url('Untitled.jpeg')">
<%if(request.getAttribute("message")==null){
	out.println("");}
else {
	out.println(request.getAttribute("message"));}%>
<div id="slider">
	
    <div id="templatemo_sidebar">
    	<div id="templatemo_header">
    	  <h1>Inventory Control System</h1>
    	</div> 
    	<!-- end of header -->
        
        <ul class="navigation">
           <li><a href="http://localhost:8080/inventoy_management/Dean/Deanhome.jsp">Home<span class="ui_icon home"></span></a></li>
            <li><a href="http://localhost:8080/inventoy_management/Dean/DeanSearch.jsp">View Received Items<span class="ui_icon aboutus"></span></a></li>
            <li><a href="http://localhost:8080/inventoy_management/Dean/DeanViewAvailable.jsp">View Available Items<span class="ui_icon services"></span></a></li>
            <li><a href="http://localhost:8080/inventoy_management/Dean/DeanRequestItem.jsp">Request Items<span class="ui_icon gallery"></span></a></li>
            <li><a href="http://localhost:8080/inventoy_management/Dean/DeanviewRequest.jsp">View Staff Request<span class="ui_icon contactus"></span></a></li>
            <li><a href="http://localhost:8080/inventoy_management/Dean/DeanProfile.jsp">Edit Profile<span class="ui_icon contactus"></span></a></li>

      </ul>
      </div>
  	<div id="templatemo_main">
    	<ul id="social_box" class="auto-style1" style="height: 22px">
            <li>Faculty Of Information Technology </li>             
        </ul>
        
        <div id="content">
        
        <!-- scroll -->
        
        	
            
            
        <!-- end of scroll -->

 <%
   String userId = (String)session.getAttribute("uname");
   if(userId == null) {
      response.sendRedirect("http://localhost:8080/inventoy_management/index.jsp");
   }
%> 	
       
      
        
        <table  style="position: absolute; left: 359px; top: 189px; width: 598px; height: 445px;">
          <tr>
            <td class="auto-style3" style="height: 8px; width: 98px">Home</td>
            <td style="width: 279px; height: 8px;" class="auto-style3"><a href="changePassword.jsp">ChangePassword</a></td>
            <td class="auto-style4" style="height: 8px">
			<span class="auto-style5"><a href="http://localhost:8080/inventoy_management/logout.jsp">Sign Out</a>
              </span>
              <% out.println(session.getAttribute("uname")); %>
              </td>
          </tr>
          <tr>
            <td colspan = "3" style="height: 125px;">
			<form method="post" action="http://localhost:8080/inventoy_management/Inventory_Search" style="position: absolute; left: 2px; top: 68px; width: 597px; height: 128px;">
              <div class="auto-style1" style="height: 122px">
                <select name="category" style="position: absolute; left: 197px; top: 27px; height: 24px; width: 122px;">
                  <option></option>
                  <option>Consumable Items</option>
                  <option>Fixed Assets</option>
                  </select>
                <input name="txtcatagory" readonly style="border-style: none; position: absolute; left: 68px; top: 28px; background-color: #201f1b; color: #FFFFFF; width: 72px;" type="text" value="Category">&nbsp;&nbsp;
                <input name="deansearch" style="position: absolute; left: 112px; top: 89px; width: 86px;" type="submit" value="Search">
               
              </div>
            </form>
              <table border=1 style="position: absolute; left: 7px; top: 207px; width: 585px;">
                <tr>
                  <th>Item</th>
                  <th>ItemCode</th>
                  <th>Quantity</th>
                  <th>Description</th>
                  </tr>
                <%try{
                	ResultSet res = (ResultSet) request.getAttribute("res");
		while(res.next()){ %>
                <tr>
                  <td><%out.print(res.getString("code")); %></td>
                  <td><%out.print(res.getString("item_name")); %></td>
                  <td><%out.print(res.getString("no_of_item")); %></td>
                  <td><%out.print(res.getString("description")); %></td>
                </tr>
                <%}}catch(Exception e){
		System.out.print("error");
	}%>
              </table>
            </td>
          </tr>
        </table>
</div> 
        <!-- end of content -->
        
        
    
    </div> 
<!-- end of main -->
</div>

</body>
</html>